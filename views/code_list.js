const express = require("express");
const router = express.Router();
const Codes = require("../models/code");

// @route   GET /codes
router.get('/', (req, res) => {

	Codes.find({})
	    .then(codes => res.json({
	    	'status': 'OK',
	    	'data': {
	    		'codes': codes
	    	}
	    }))
	    .catch(err =>
	      res.status(200).json({ 
	      	'status': 'ZERO_RESULTS',
	      	'data': {}
	      })
    );

})

// @rout GET /codes/:id
router.get("/:id", (req, res) => {

	Codes.findById(req.params.id)
		.then(code => res.json({
			'status': 'OK',
			'data': code
		}))
		.catch(err =>
	  		res.status(404).json({
	  			'status': 'ZERO_RESULTS',
	  			'data': {}
	  		})
	);

});

// POST method route
router.post('/', (req, res) => {

	context = {
		'status': 'OK',
		'data': {}
	}
	text = req.body.text
	type = req.body.type

	if (!text){
		context.status = 'INVALID_REQUEST'
		return res.status(400).json(context);
	}
	if (!type){
		context.status = 'INVALID_REQUEST'
		return res.status(400).json(context);
	}

	const newCode = new Codes({
		text: req.body.text,
		type: req.body.type
    });
    newCode.save().then(code => res.json(code));

})

module.exports = router;