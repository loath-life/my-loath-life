// Settings
const bodyParser = require('body-parser');
const express = require('express')
const code = require("./views/code_list");
const mongoose = require("mongoose");
const app = express()
const MONGO_URI = require("./settings.js").MONGO_URI.trim();
const user = require("./settings.js").MONGO_DB_USER.trim();
const password = require("./settings.js").MONGO_DB_PASSWORD.trim();
const port = 3000

mongoose
  .connect(`mongodb://${MONGO_URI}/loath`, {
  	auth: {
  		user: user,
  		password: password,
  		useMongoClient: true
  	}, 
	useNewUrlParser: true,
	useUnifiedTopology: true
  })
  .then(() => console.log("Mongo DB Connected."))
  .catch(err => console.log(err));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// Routes
app.use("/codes", code);

app.listen(port, () => console.log(`Example app listening on port ${port}!`))