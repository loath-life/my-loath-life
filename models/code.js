const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let Code;

const CodeModel = new Schema({
  text: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Code = mongoose.model("code", CodeModel);
