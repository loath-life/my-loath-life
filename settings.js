// Exports
//require('.env')
module.exports = {
	MONGO_URI: process.env.MONGO_URI,
	MONGO_DB_USER: process.env.MONGO_DB_USER,
	MONGO_DB_PASSWORD: process.env.MONGO_DB_PASSWORD
};
